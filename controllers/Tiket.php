<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// Jika ada pesan "REST_Controller not found"
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Tiket extends REST_Controller {

	// Konfigurasi letak folder untuk upload image
	private $folder_upload = 'uploads/';

	function all_get(){
		$get_tiket = $this->db->query("
			SELECT 
				id_tiket, 
				tujuan, 
				tanggal_berangkat, 
				nama_kereta
			FROM tiket")->result();
        $this->response(
        	array(
        		"status" => "success",
        		"result" => $get_tiket
        	)
        );
	}

	function all_post() {

		$action  = $this->post('action');
		$data_tiket = array(
		                'id_tiket' => $this->post('id_tiket'),
		                'tujuan'       => $this->post('tujuan'),
		                'tanggal_berangkat'     => $this->post('tanggal_berangkat'),
		                'nama_kereta'      => $this->post('nama_kereta')
	                );

		switch ($action) {
			case 'insert':
				$this->insertTiket($data_tiket);
				break;
			
			case 'update':
				$this->updateTiket($data_tiket);
				break;
			
			case 'delete':
				$this->deleteTiket($data_tiket);
				break;
			
			default:
				$this->response(
					array(
						"status"  =>"failed",
						"message" => "action harus diisi"
					)
				);
				break;
		}
	}

	function available_post(){

		$id_pembeli = $this->post("id_pembeli");

		$get_tiket = $this->db->query("
			SELECT	
				t.id_tiket, 
				t.tujuan, 
				DATE_FORMAT(tanggal_berangkat, '%e %b %Y') as tgl_brgkt, 
				t.nama_kereta,
				t.kuota,
				FORMAT(t.harga, 0, 'id_ID') as harga_tiket
			FROM tiket t
			WHERE tanggal_berangkat > CURDATE() 
				AND kuota > 0 
				AND t.id_tiket NOT IN (
					SELECT id_tiket
					FROM pembelian
					WHERE id_pembeli = {$id_pembeli}
				)
			ORDER BY tanggal_berangkat ASC")->result();
        $this->response(
        	array(
        		"status" => "success",
        		"result" => $get_tiket
        	)
        );
	}

	function mytiket_post(){

		$id_pembeli = $this->post("id_pembeli");

		$get_tiket = $this->db->query("
			SELECT	
				t.id_tiket, 
				t.tujuan, 
				DATE_FORMAT(tanggal_berangkat, '%e %b %Y') as tgl_brgkt, 
				t.nama_kereta,
				t.kuota,
				FORMAT(t.harga, 0, 'id_ID') as harga_tiket,
				DATE_FORMAT(tanggal_beli, '%e %b %Y') as tgl_beli
			FROM tiket t
			INNER JOIN pembelian p
				ON p.id_tiket = t.id_tiket
			WHERE p.id_pembeli = {$id_pembeli} AND p.status = 1
			ORDER BY tanggal_berangkat ASC")->result();
        $this->response(
        	array(
        		"status" => "success",
        		"result" => $get_tiket
        	)
        );
	}

	function insertTiket($data_tiket){

	    // Cek validasi
	    if (empty($data_tiket['tujuan']) || empty($data_tiket['tanggal_berangkat']) || empty($data_tiket['nama_kereta'])){
	    	$this->response(
	    		array(
	    			"status" => "failed",
	    			"message" => "Tujuan / tgl berangkat / nama kereta harus diisi"
	    		)
	    	);
	    } else {

	    	// $data_tiket['photo_url'] = $this->uploadPhoto();

	    	$do_insert = $this->db->insert('tiket', $data_tiket);
		    
		    if ($do_insert){
			    $this->response(
			    	array(
			    		"status" => "success",
			    		"result" => array($data_tiket),
			    		"message" => $do_insert
			    	)
			    );
			} else {
				$this->response(
			    	array(
			    		"status" => "failed",
			    		"result" => array($data_tiket),
			    		"message" => "DB insert gagal"
			    	)
			    );
			}
	    }
	}

	function updateTiket($data_tiket){

	    // Cek validasi
	    if (empty($data_tiket['tujuan']) || empty($data_tiket['tanggal_berangkat']) || empty($data_tiket['nama_kereta'])){
	    	$this->response(
	    		array(
	    			"status" => "failed",
	    			"message" => "Tujuan / tgl berangkat / nama kereta harus diisi"
	    		)
	    	);
	    } else {
	    	// Cek apakah ada di database
	    	$get_tiket_baseID = $this->db->query("
	    		SELECT 1 
	    		FROM tiket 
	    		WHERE id_tiket =  {$data_tiket['id_tiket']}")->num_rows();

	    	if($get_tiket_baseID === 0){
		    	// Jika tidak ada
		    	$this->response(
		    		array(
		    			"status"  => "failed", 
		    			"message" => "ID tiket tidak ditemukan"
		    		)
		    	);
	    	} else {
	    		// Jika ada

				$update = $this->db->query("
					UPDATE tiket 
					SET 
						tujuan 	= '{$data_tiket['tujuan']}',
						tanggal_berangkat 	= '{$data_tiket['tanggal_berangkat']}',
						nama_kereta 	= '{$data_tiket['nama_kereta']}'
					WHERE id_tiket = {$data_tiket['id_tiket']}"
				);
			    
			    if ($update){
				    $this->response(
				    	array(
				    		"status" 	=> "success",
				    		"result" 	=> array($data_tiket),
				    		"message" 	=> $update
				    	)
				    );
				} else {
					$this->response(
						array(
							"status"  => "failed", 
							"message" => $this->db->error_get_last()
						)
					);
				}
	    	}    
	    }
	}

	function deleteTiket($data_tiket){

		if (empty($data_tiket['id_tiket'])){
	    	$this->response(
	    		array(
	    			"status" => "failed", 
	    			"message" => "ID tiket harus diisi"
	    		)
	    	);
	    } else {
	    	// Cek apakah ada di database
	    	$get_tiket_baseID =$this->db->query("
	    		SELECT 1 
	    		FROM tiket 
	    		WHERE id_tiket = {$data_tiket['id_tiket']}")->num_rows();

	    	if($get_tiket_baseID > 0){

			    	$delete = $this->db->query("
			    		DELETE FROM tiket 
			    		WHERE id_tiket = {$data_tiket['id_tiket']}");

			    	if($delete) {
				    	$this->response(
				    		array(
				    			"status" => "success",
				    			"message" => "Data ID = " .$data_tiket['id_tiket']. " berhasil dihapus"
				    		)
				    	);
			    	} else {
			    		$error = $this->db->error();
						$this->response(
							array(
								"status"  => "failed", 
								"message" => $error['message']
							)
						);
			    	}
	    	
			} else {
				$this->response(
					array(
						"status" => "failed",
						"message" => "ID tiket tidak ditemukan"
					)
				);
			}
	    }
	}

	function uploadPhoto() {

		// Apakah user upload gambar?
		if ( isset($_FILES['photo_url']) && $_FILES['photo_url']['size'] > 0 ){

			// Foto disimpan di android-api/uploads
			$config['upload_path'] = realpath(FCPATH . $this->folder_upload);
			$config['allowed_types'] = 'jpg|png';

	        // Load library upload & helper
	        $this->load->library('upload', $config);
	        $this->load->helper('url');

	        // Apakah file berhasil diupload?
	        if ( $this->upload->do_upload('photo_url')) {

        		// Berhasil, simpan nama file-nya
        		// URL image yang disimpan adalah http://localhost/android-api/uploads/namafile
        	    $img_data = $this->upload->data();
        	    $post_image = base_url(). $this->folder_upload .$img_data['file_name'];

	        } else {

	        	// Upload gagal, beri nama image dengan errornya
	        	// Ini bodoh, tapi efektif
	        	$post_image = $this->upload->display_errors();
	        	
	        }
	    } else {
	    	// Tidak ada file yang di-upload, kosongkan nama image-nya
	    	$post_image = '';

	    }

	    return $post_image;
	} 
}
